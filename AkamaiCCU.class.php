<?php
/**
 * Akamai Content Control Utility
 * @id: $Id: AkamaiCCU.class.php 39019 2009-04-16 20:51:55Z apotek $
 *
 * A class hierarchy for sending commands to the Akamai Content Control
 * API, enabling PHP sites to invalidate or remove cached files from
 * the CDN
 *
 * @author Apotek
 * @since 5.2.4
 * @version 1.0
 *
 * @todo Implement set_file_types() or set_directories() and instantiate
 * a proper ECCU client rather than a CCU client for such items.
 */

/**
 * AkamaiCCU (Content Control Utility) is the only public-facing class
 * in this package. It will return an instance of a child-class,
 * depending on the type of CCU requested (standard, or enhanced).
 *
 * Presents a standardized interface to handle Akamai content control
 * requests.
 * Example:
 *  $myCCU = new AkamaiCCU;
 *  //At a minimum, you must specify a name and password
 *  $myCCU->set_options(array(
 *    "name"  =>  [akamai login name],//REQUIRED
 *    "pwd"   =>  [akamai password],//REQUIRED
 *    "email" =>  [email address for notifications],
 *    "domain"=>  [AkamaiCCU::DOMAIN_PROD (default),
 *                or AkamaiCCU::DOMAIN_STAGING]
 *  ));
 *  $myCCU->set_items(
 *    array([items to submit]),
 *    [AkamaiCCU::ITEM_TYPE_ARL or AkamaiCCU::ITEM_TYPE_CPCODE]
 *  );
 *  $myCCU->invalidate(); OR $myCCU->remove();
 *
 */
class AkamaiCCU
{
  const CLIENT_TYPE_STANDARD='CCU';
  const CLIENT_TYPE_ENHANCED='ECCU';
  const ITEM_TYPE_ARL='arl';
  const ITEM_TYPE_CPCODE='cpcode';
  const ITEM_TYPE_EXTENSION_MATCH='ext';
  const ITEM_TYPE_DIRECTORY='dir';
  const DOMAIN_PROD='production';
  const DOMAIN_STAGING='staging';

  const ACTION_INVALIDATE = 'invalidate';
  const ACTION_REMOVE = 'remove';

  const USAGE = '
  USAGE:
  $myCCU = new AkamaiCCU;
  //At a minimum, you must specify a name and password
  $myCCU->set_options(array(
    "name"  =>  [akamai login name],//REQUIRED
    "pwd"   =>  [akamai password],//REQUIRED
    "email" =>  [email address for notifications],
    "domain"=>  [AkamaiCCU::DOMAIN_PROD (default),
                 or AkamaiCCU::DOMAIN_STAGING]
  ));
  $myCCU->set_items(
    array([items to submit]),
    [$myCCU::ITEM_TYPE_ARL or $myCCU::ITEM_TYPE_CPCODE]
  );
  $myCCU->invalidate(); OR $myCCU->remove();


';
  /** @var bool $debug will keep debugging state for object
   * @access private */
  private $debug;
  /** @var object $client will contain the extended SoapClient: Either
   * an instance of AkamaiCCUClient or AkamaiECCUClient */
  var $client;
  /** @var string $_client_type stores whether the client to be
   * instantiated should be the Standard client (AkamaiCCUClient) or
   * the enhanced client (AkamaiECCUClient).
   * @access private
   */
  private $_client_type;
  /** @var object $result will contain the SoapResult
   * @access private */
  private $result;
  /** @var array $log contains the log messages generated during use of
   * the AkamaiCCU object
   * @access private */
  private $log = array();
  /**
   * @var array $_items An array of CPCODEs or ARLs to purge from the
   * Akamai CDN. Use set_items to populate this.
   * @access private
   */
  private $_items = array();
  /** @var array $_exts contains the extensions to be sent to
   * Akamai for purging */
  protected $_exts = array();
  /** @var array $_dirs contains the directories to be sent to
   * Akamai for purging */
  protected $_dirs = array();

  /** @var string $name will contain Akamai user name */
  protected $name;
  /** @var string $pwd will contain Akamai password for the user */
  protected $pwd;
  /** @var string $email will contain email notification address */
  protected $email;
  /** @var string $domain will store whether this is a production or
   * staging request. */
  protected $domain;

  /** Prevents users from cloning the instance in case abstract isn't
   * enforced.
   * @access public
   */
  public function __clone() {
    trigger_error('err cannot clone '. __CLASS__, E_USER_ERROR);
  }

  /**
   * Returns an instance of an AkamaiCCU class. By default, it
   * @param bool $debug Whether to increase diagnostic output during
   * use of the class. Default is FALSE.
   * @return object An instance of the AkamaiCCU class
   * @access public
   */
  public function __construct($debug=FALSE) {
    //set some defaults
    $this->domain = self::DOMAIN_PROD;
    $this->_client_type = self::CLIENT_TYPE_STANDARD;

    $this->debug = $debug;
  }

  /**
   * Writes the passed message to the AkamaiCCU's log variable
   * @param string $msg The error/log message to write
   * @param int $err_level A defined PHP error level constant. The
   * default is NULL. If an error level is passed, trigger_error is also
   * called.
   * @access protected
   */
  protected function _log($msg, $err_level=E_USER_NOTICE) {
    $this->log[] = $msg;
    if ($this->debug) {
      trigger_error($msg, $err_level);
    } else {
      switch($err_level) {
        case E_USER_WARNING:
          error_log($msg);
          echo $msg;
          break;
        case E_USER_ERROR:
          trigger_error($msg, $err_level);
          break;
      }
    }
  }


  /**
   * Purges the set items, using the method set in remove() or
   * invalidate(). If no items have been set, the method outputs an
   * error message.
   * @return an Akamai CCU result object. The Akamai result object has
   * the following structure:
   * ->resultCode (success: 100, failure: 3nn)
   * ->resultMsg (An english result statement)
   * ->sessionID (The session id of the request, for troubleshooting)
   * ->estTime (not sure what this is for)
   * ->uriIndex (not sure what this is for)
   * ->modifiers (not sure what this is for)
   * ->lastResponse (If debug is set, this will contain all the XML of
   * the response received from Akamai)
   *
   * @access private
   * @see AkamaiSCCU->invalidate(), AkamaiSCCU->remove()
   */
  private function _purge($action=self::ACTION_INVALIDATE) {

    if (empty($this->_items) && empty($this->_exts) && empty($this->_dirs)) {
      $this->_log('No items, extensions, or directories have been set for the CCU to '. $action .'. Please use methods set_items(), set_extensions(), or set_directories().' ."\n". self::USAGE, E_USER_WARNING);
      return FALSE;
    }

    if (empty($this->name) || empty($this->pwd)) {
      $this->_log('You must specify at least a name and password' ."\n". self::USAGE, E_USER_ERROR);
      return FALSE;
    }

    if ($this->_client_type==self::CLIENT_TYPE_ENHANCED) {//Use Enhanced CCU client
      /**
       * @todo write the setup logic for using the enhanced client
       * this includes setting any options, and setting up the
       * $params array for the client
       */
      $params = array();
      $this->client = new AkamaiECCUClient($this->debug);
    } else {//use Standard CCU client
      $opts = array(
        "domain=$this->domain", //set in constructor and/or set_options()
        "type=$this->type", //set in set_items()
        "action=$action"//set in function call
      );
      if (!empty($this->email)) {//no default value, set in set_options
        $opts[] = "email-notification=$this->email";
      }

      /*
       * http://www.php.net/manual/ro/soapclient.soapcall.php#82049
       */
      $params = array(
        new SoapParam($this->name,'name'),
        new SoapParam($this->pwd,'pwd'),
        new SoapParam($opts, 'opt'),
        new SoapParam($this->_items, 'uri')
      );
      $this->client = new AkamaiCCUClient($this->debug);
    }

    try{
      $this->result = $this->client->purgeRequest($params);
      if ($this->debug) {
        ob_start();
        var_dump($this->result);
        $a = ob_get_contents();
        ob_end_clean();
        error_log($a);
        $this->result->lastRequest = $this->client->__getLastRequest();
        $this->result->lastResponse = $this->client->__getLastResponse();
      }

      /*
       * Result Code Element Indicates
       * 1xx Successful Request
       * 2xx Warning; reserved. The removal request has been accepted.
       * 3xx Bad or invalid request.
       * 4xx Contact Akamai Customer Care. */
      if ($this->result->resultCode<200) {
        $el = E_USER_NOTICE;
      } else {
        $el = E_USER_WARNING;
      }
      $this->_log($this->result->resultMsg, $el);
      //1xx or 2xx is an accepted request
      if ($this->result->resultCode<300) {
        return TRUE;
      }
      return FALSE;
    }
    catch(Exception $e) {
      $msg = 'Soap Error: '. $e->getMessage(). "\n";
      if ($this->debug) {
        echo $msg;
        debug_print_backtrace();
      }
      $this->_log($msg, E_USER_ERROR);
      exit();
    }
  }

  /**
   * Sets the items that should be submitted to Akamai for purging
   *
   * @param array $items An array of strings, with either fully quali-
   * fied ARLs or CPCODEs.
   * @param string $type Specifies whether the passed items are ARLs or
   * CPCODEs. The default is AkamaiCCU::ITEM_TYPE_ARL, but if the user
   * is submitting CPCODEs, it should be set to
   * AkamaiCCU::ITEM_TYPE_CPCODE
   * @return int The number of items successfully added to the list.
   */
  public function set_items(array $items, $type=self::ITEM_TYPE_ARL) {
    $this->type = $type;

    $bad_items = array();
    while(list(,$item) = each($items)) {
      if (is_string($item)) {
        $this->_items[] = $item;
      } else {
        $bad_items[] = $item;
      }
    }
    if (!empty($bad_items)) {
      $msg = '';
      while(list(,$item) = each($bad_items)) {
        $msg .= "\t$item\n";
      }
      $this->_log("The following items are invalid and will not be submitted to akamai:\n$msg", E_USER_NOTICE);
    }
    return count($this->_items);
  }


  /**
   * Sets the directories that should be submitted to Akamai for purging
   *
   * @param array $directories An array of strings containing fully
   * qualified ARLs to directories that should be recursively purged
   * @return int The number of items successfully added to the list.
   */
  public function set_directories(array $directories) {
    $this->_client_type = self::CLIENT_TYPE_ENHANCED;
    trigger_error('err '. __FUNCTION__ .' not yet implemented', E_USER_ERROR);
    return FALSE;
    /* @todo validate dirs and add only valid directory patterns to the
     * _dirs array */
    if (!empty($this->_dirs)) {
      $this->_client_type = self::CLIENT_TYPE_ENHANCED;
    }
    return (count($this->_dirs));
  }

  /**
   * Sets the file extensions that should be submitted to Akamai for
   * purging
   *
   * @param array $extensions An array of strings containing the
   * file extensions that should be purged on a given property
   *
   * @return int The number of items successfully added to the list.
   */
  public function set_extensions(array $extensions) {
    trigger_error('err '. __FUNCTION__ .' not yet implemented', E_USER_ERROR);
    return FALSE;
    /* @todo validate extensions and add only valid extension patterns
     * to the _exts array */
    if (!empty($this->_exts)) {
      $this->_client_type = self::CLIENT_TYPE_ENHANCED;
    }
    return (count($this->_exts));
  }


  /**
   * Sets the soap request options (opt param)
   *
   * @param array $options An array of key=>values to be set for the
   * soap request. The array should look like this:
   * array(
   *  'name' => [akamai user name],//required
   *  'pwd' => [akamai user password],//required
   *  'email' => [email address for notifications],//optional
   *  //optional
   *  'domain' => [AkamaiCCU:DOMAIN_PROD or AkamaiCCU::DOMAIN_STAGING]
   * )
   * @return TRUE if options were set successfully, FALSE if not
   */
  public function set_options(array $options) {
    if (!isset($options['name']) || !isset($options['pwd'])) {
      $this->_log('You must specify at least a name and password' ."\n". self::USAGE, E_USER_ERROR);
      return FALSE;
    }
    $this->name = $options['name'];
    $this->pwd = $options['pwd'];
    if (isset($options['email'])) {
      $this->email = $options['email'];
    }
    if (isset($options['domain'])) {
      $this->domain = $options['domain'];
    }
    return TRUE;
  }

  /**
   * Invalidates the previously set cached items in the CDN
   * @return an Akamai CCU result object
   * @access public
   */
  public function invalidate() {
    return $this->_purge(self::ACTION_INVALIDATE);
  }

  /**
   * Removes the previously set cached items from the CDN
   * @return an Akamai CCU result object
   * @access public
   */
  public function remove() {
    return $this->_purge(self::ACTION_REMOVE);
  }


  /**
   * Returns an array of logged messages generated and stored while
   * using the AkamaiCCU class. Each message is stored in a separate
   * element of the array. To output as a readable collection of text,
   * just do print implode("\n", $myCCU->get_log());
   * @access public
   * @return array An array of messages generated during the operation
   * of the AkamaiCCU class
   */
  public function get_log() {
    return $this->log;
  }

  /**
   * Returns the result code integer returned by Akamai on latest
   * operation.
   * @return int The akamai defined result code for the latest purge
   * request. If there has been no result, 0 will be returned.
   * @access public */
  public function get_result_code() {
    if ($this->result) {
      return $this->result->resultCode;
    }
    return 0;
  }

  /**
   * Returns the result message returned by Akamai on latest operation.
   * @return string The plain english result string for the latest purge
   * request.
   * @access public */
  public function get_result_msg() {
    if ($this->result) {
      return $this->result->resultMsg;
    }
    return '';
  }
}

/**
 * AkamaiSoapClient interface sets up required methods for the
 * AkamaiCCU SoapClient extension classes
 *
 * @interface
 * @package com.logoonline.utils.akamai
 * @author Kristofer Widholm, Logo Online
 * @since 5.2.4
 * @version 1
 */
interface AkamaiSoapClient
{
  public function purgeRequest(array $params);
}

/**
 * AkamaiCCUClient (Content Control Utility Client) handles requests for
 * old-school content control utility requests: lists of cpcodes or
 * ARLs, with no pattern matching available.
 *
 * @see AkamaiCCU->_purge() for example usage
 *
 * @package com.logoonline.utils.akamai
 * @author Kristofer Widholm, Logo Online
 * @since 5.2.4
 * @version 1
 */
class AkamaiCCUClient extends SoapClient implements AkamaiSoapClient
{
  /** @const string LOCATION The URL of the soap call destination */
  const LOCATION = 'https://ccuapi.akamai.com:443/soap/servlet/soap/purge';
# const LOCATION = 'http://localhost:9999/soap.php';
  /** @const string URI the url of the web services description */
  const URI = 'http://ccuapi.akamai.com/purge';
  /** @var array $_options Will hold the options passed to the
   * SoapClient
   * @access private */
  private $_options = array();
  /**
   * @var string $_network The Akamai-specific network variable. It
   * seems to always be set to ff.
   * @access private
   */
  private $_network = 'ff';
  /** @var bool $debug will keep debugging state for object
   * @access private */
  private $debug;

  /**
   * constructor for the AkamaiCCUClient class
   * @param bool $debug Set debug on (TRUE), or off (FALSE). Default is
   * FALSE
   * @param array $params An array of parameters to pass to the client.
   */
  public function __construct($debug=FALSE) {
    $this->debug = $debug;

    $this->_options = array(
      'location' => self::LOCATION,
      'uri' => self::URI
    );
    if ($this->debug) {
      $this->_options['trace'] = TRUE;
      $this->_options['exceptions'] = TRUE;
    }

    parent::__construct(NULL, $this->_options);
  }

  public function purgeRequest(array $params) {
    //add network param
    $params[] = new SoapParam($this->_network,'network');
    return $this->__soapCall('purgeRequest', $params);
  }

}

/**
 * AkamaiCCUClient (Content Control Utility Client) handles requests for
 * old-school content control utility requests: lists of cpcodes or
 * ARLs, with no pattern matching available.
 *
 * @see AkamaiCCU->_purge() for example usage
 *
 * @since 5.2.4
 * @version 0
 * @todo, build this class
 */
class AkamaiECCUClient extends SoapClient implements AkamaiSoapClient
{
  /** @var bool $debug will keep debugging state for object
   * @access private */
  private $debug;

  public function __construct($debug=FALSE) {
    //don't let anyone use this for now.
    trigger_error('err '. __CLASS__ .' not yet implemented', E_USER_ERROR);

    $this->debug = $debug;
    parent::__construct();
  }

  /**
   * @todo document once this gets written
   */
  public function purgeRequest(array $params) {
    return $this->__soapCall('purgeRequest', $params);
  }

}
