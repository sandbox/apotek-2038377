<?php
/** @rev: $Revision: 61594 $, $LastChangedBy: apotek $ */
/**
 * Return data from the persistent cache.
 *
 * @param $key
 *   The cache ID of the data to retrieve.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache' for the default cache.
 */
function cache_get($key, $table = 'cache') {
  global $user;

  /** @logomod, LOGOT-125, LOGOT-145: Site stability: eliminate cache clear contention */
  /** @logomod, LOGOT-42: memcache implementation
  $cache = db_fetch_object(db_query("SELECT data, created, headers, expire FROM {". $table ."} WHERE cid = '%s'", $key));*/
  $cache = db_fetch_object(db_query("SELECT data, created, headers, expire, serialized FROM {%s} WHERE cid = '%s'", $table, $key));
  if (isset($cache->data)) {
    // If the data is permanent or we're not enforcing a minimum cache lifetime
    // always return the cached data.
    $cache_lifetime = variable_get('cache_lifetime', 0);
    if ($cache->expire == CACHE_PERMANENT || !$cache_lifetime) {
      $cache->data = db_decode_blob($cache->data);
    /** @logomod, LOGOT-42: memcache implementation */
    if ($cache->serialized) {
        $cache->data = unserialize($cache->data);
    }
    /** end logomod */
  }
    // If enforcing a minimum cache lifetime, validate that the data is
    // currently valid for this user before we return it by making sure the
    // cache entry was created before the timestamp in the current session's
    // cache timer. The cache variable is loaded into the $user object by
    // sess_read() in session.inc.
    else {
      /** @logomod, LOGOT-125, LOGOT-145: Site stability: eliminate cache clear contention */
      // $cache->expire should always be set in the future, so if less than now, its expired
      // is this bad code? if expire is set to now in cache_set(), then below may always invalidate the data,
      // causing massive database load.
      // if ($cache->expire <= (time() - $cache_lifetime)) {
      if ($cache->expire <= time()) {
        // The cache data is too old, so return 0 so new data can be created
        return 0;
      }
      // end logomod

    if ($user->cache > $cache->created) {
        // This cache data is too old and thus not valid for us, ignore it.
        return 0;
      }
      else {
        $cache->data = db_decode_blob($cache->data);
    /** @logomod, LOGOT-42: memcache implementation */
    if ($cache->serialized) {
      $cache->data = unserialize($cache->data);
    }
    /** end logomod */
      }
    }
    return $cache;
  }
  return 0;
}

/**
 * Store data in the persistent cache.
 *
 * The persistent cache is split up into four database
 * tables. Contributed modules can add additional tables.
 *
 * 'cache_page': This table stores generated pages for anonymous
 * users. This is the only table affected by the page cache setting on
 * the administrator panel.
 *
 * 'cache_menu': Stores the cachable part of the users' menus.
 *
 * 'cache_filter': Stores filtered pieces of content. This table is
 * periodically cleared of stale entries by cron.
 *
 * 'cache': Generic cache storage table.
 *
 * The reasons for having several tables are as follows:
 *
 * - smaller tables allow for faster selects and inserts
 * - we try to put fast changing cache items and rather static
 *   ones into different tables. The effect is that only the fast
 *   changing tables will need a lot of writes to disk. The more
 *   static tables will also be better cachable with MySQL's query cache
 *
 * @param $cid
 *   The cache ID of the data to store.
 * @param $data
 *   The data to store in the cache. Complex data types will be automatically serialized before insertion.
 *   Strings will be stored as plain text and not serialized.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache'.
 * @param $expire
 *   One of the following values:
 *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
 *     explicitly told to using cache_clear_all() with a cache ID.
 *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
 *     general cache wipe.
 *   - A Unix timestamp: Indicates that the item should be kept at least until
 *     the given time, after which it behaves like CACHE_TEMPORARY.
 * @param $headers
 *   A string containing HTTP header information for cached pages.
 */
function cache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL) {
/** @logomod eb: CACHE_TEMPORARY replaced by expire */
  $now = time();
  //if temporary, instead set to expire x seconds from now where x == cache_lifetime
  if ($expire == CACHE_TEMPORARY) {
    $expire = $now + variable_get('cache_lifetime', 900);
  }

/** @logomod, LOGOT-42: memcache implementation */
  $serialized = 0;
  if (is_object($data) || is_array($data)) {
    $data = serialize($data);
    $serialized = 1;
  }

/**
 * @logomod, LOGOT-42: memcache implementation added through addition of serialized variable
 *
 * @logomod Kristofer, r9: Removing table locks as per suggested patches to Drupal core
 * as found in http://drupal.org/files/issues/no_locks_vs_5.patch.*/
  db_query("UPDATE {". $table. "} SET data = %b, created = %d, expire = %d, headers = '%s', serialized = %d WHERE cid = '%s'", $data, $now, $expire, $headers, $serialized, $cid);
  if (!db_affected_rows()) {
    @db_query("INSERT INTO {". $table. "} (cid, data, created, expire, headers, serialized) VALUES ('%s', %b, %d, %d, '%s', %d)", $cid, $data, $now, $expire, $headers, $serialized);
  }
}

/**
 *
 * Expire data from the cache. If called without arguments, expirable
 * entries will be cleared from the cache_page table.
 *
 * @param $cid
 *   If set, the cache ID to delete. Otherwise, all cache entries that can
 *   expire are deleted.
 *
 * @param $table
 *   If set, the table $table to delete from. Mandatory
 *   argument if $cid is set.
 *
 * @param $wildcard
 *   If set to TRUE, the $cid is treated as a substring
 *   to match rather than a complete ID. The match is a right hand
 *   match. If '*' is given as $cid, the table $table will be emptied.
 */
/*
 @logomod, LOGOT-334: - EB - Temporary source param addition
*/
function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
  global $user;

  if (!isset($cid) && !isset($table)) {
    cache_clear_all(NULL, 'cache_page');
    return;
  }

  if (empty($cid)) {
    $cache_lifetime = variable_get('cache_lifetime', 0);
    $now = time();
    if ($cache_lifetime) {
      // We store the time in the current user's $user->cache variable which
      // will be saved into the sessions table by sess_write(). We then
      // simulate that the cache was flushed for this user by not returning
      // cached data that was cached before the timestamp.
      $user->cache = $now;

      $cache_flush = variable_get('cache_flush', 0);
      if ($cache_flush == 0) {
        // This is the first request to clear the cache, start a timer.
        variable_set('cache_flush', $now);
      }
      else if ($now > ($cache_flush + $cache_lifetime)) {
    // Clear the cache for everyone, cache_flush_delay seconds have
    // passed since the first request to clear the cache.
    db_query("DELETE FROM {". $table. "} WHERE expire != %d AND expire < %d", CACHE_PERMANENT, $now);
        variable_set('cache_flush', 0);
      }
    }
    else {
      // No minimum cache lifetime, flush all temporary cache entries now.
      db_query("DELETE FROM {". $table. "} WHERE expire != %d AND expire < %d", CACHE_PERMANENT, $now);
    }
  }
  else {
    if ($wildcard) {
      if ($cid == '*') {
        db_query("DELETE FROM {". $table. "}");
      }
      else {
        db_query("DELETE FROM {". $table. "} WHERE cid LIKE '%s%%'", $cid);
      }
    }
    else {
      db_query("DELETE FROM {". $table. "} WHERE cid = '%s'", $cid);
    }
  }
}
